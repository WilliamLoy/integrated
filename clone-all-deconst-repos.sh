#!/usr/bin/env bash
# based on https://gist.github.com/JonasGroeger/1b5155e461036b557d0fb4b3307e1e75

set -eou pipefail

GROUP_NAME="deconst-next"
FIELD_NAME="ssh_url_to_repo"

echo "This script clones or pulls all projects in the $GROUP_NAME group" 
echo "(You must first upload your SSH key to your account on gitlab.com)"
echo ""
echo "Changing to $GROUP_NAME directory"
mkdir -p $GROUP_NAME
pushd $GROUP_NAME

REPO_SSH_URLS=`curl -s "https://gitlab.com/api/v4/groups/$GROUP_NAME/projects?per_page=99" \
   | grep -o "\"$FIELD_NAME\":[^ ,]\+" | awk -F'"' '{print $4}' | grep $GROUP_NAME`

for REPO_SSH_URL in $REPO_SSH_URLS; do
    THEPATH=$(echo "$REPO_SSH_URL" | awk -F'/' '{print $NF}' | awk -F'.' '{print $1}')

    if [ ! -d "$THEPATH" ]; then
        echo "Cloning $THEPATH"
        git clone "$REPO_SSH_URL" --quiet &
    else
        echo "Pulling $THEPATH"
        (cd "$THEPATH" && git pull --quiet) &
    fi
done
